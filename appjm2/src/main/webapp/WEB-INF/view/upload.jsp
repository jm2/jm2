<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Administracion</title>
	</head>
	<body>
		
		<form method="POST" action="/app/saveBuild" enctype="multipart/form-data">
			Precio: <input name="price" /> <br />
			Descripcion: <input name="comment" /> <br />
			Estado: <input name="state" /> <br />
			Tipo: <input name="type" /> <br />
			Localizacion: <input name="location" /> <br />
			Caracteristicas: <input name="features"> <br />
			Equipamientos: <input name="equipments" /> <br />
			Imagen principal: <input type="file" name="file" /> <br />
			Imagenes: <input type="file" name="files" multiple="multiple" /> <br />
			<input type="submit" value="Aceptar" />
		</form>
		
	</body>
</html>