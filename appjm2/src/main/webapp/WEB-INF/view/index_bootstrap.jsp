<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/main.css">
        
        <title>JM2 Inmobiliaria</title>
    </head>
    
    <body>
        <nav class="navbar navbar-default" style="background-color: #43a047;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar white"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#index" data-toggle="tab">
                        <span class="sp-logo">JM2</span>
                    </a>
                </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="white" href="#whoweare" data-toggle="tab">
                                Quienes somos
                            </a>
                        </li>
                        <li>
                            <a href="#builds" data-toggle="tab">
                                Inmbuebles
                            </a>
                        </li>
                        <li>
                            <a href="#contact" data-toggle="tab">
                                Contactar
                            </a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        
        <div class="layout">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in" id="index">
                    
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="whoweare">
                    Somos un grupo de profesionales con una dilatada experiencia inmobiliaria y jur&iacutedica. <br /><br />
                    Acompa&ntildeamos a nuestros clientes durante todo el proceso de compra-venta, alquiler o traspasos de su casa local oficina, nave, terreno rustico etc, asesor&aacutendoles y apoy&aacutendoles en todos los aspectos inmobiliarios y jur&iacutedicos hasta que se concluya la operaci&oacuten. <br /><br />
                    Adem&aacutes tambi&eacuten ofrecemos certificaciones energ&eacuteticas sin intermediarios y con precios muy econ&oacutemicos.

                </div>
                <div role="tabpanel" class="tab-pane fade in active" id="builds">
					<div class="form-search">
						<form id="form-search">
							<div class="form-group">
								<label for="type">Tipo</label>
								<select id="type" class="form-control" name="type">
									<option>Piso</option>
									<option>Chalet</option>
								</select>
							</div>
							<div class="form-group">
								<label for="location">Localizacion</label>
								<select id="location" class="form-control" name="location">
									<option>Madrid</option>
								</select>
							</div>
							<div class="form-group">
								<label for="state">Estado</label>
								<select id="state" class="form-control" name="state">
									<option>Alquiler</option>
								</select>
							</div>
							<div class="form-group">
								<label for="min-price">Precio minimo</label>
								<input id="min-price" type="number" name="priceMin" />
							</div>
							<div class="form-group">
								<label for="max-price">Precio m&aacuteximo</label>
								<input id="max-price" type="number" name="priceMax" />
							</div>
							
							<input type="submit" value="Buscar" />
						</form>
					</div>
					<div id="searched-builds"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="contact">
                    Sector Oficios 6 y 7. Tres Cantos (Madrid). <br />
                    <span class="lbl">Email:</span> info@jm2inmobiliaria.es <br />
                    <span class="lbl">Telefono fijo:</span> 91 804 26 25 <br />
                    <span class="lbl">Telefono movil:</span> 606 45 44 82 <br />
                </div>
            </div>
        </div>
        
        <script src="static/js/jquery.js"></script>
        <script src="static/bootstrap/js/bootstrap.js"></script>
        
        <script src="static/js/builds.js"></script>
    </body>
</html>