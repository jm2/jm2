<!DOCTYPE>
<html>

    <head>
        <link rel="import" href="static/elements/controller/app-controller.html">
        
        <script>
        	var contextPath = '${pageContext.request.contextPath}';
        </script>
        
        <script src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
        <script>
            $.fn.serializeObject = function()
            {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        </script>
    </head>
    
    <body>
        
        <app-controller></app-controller>
    
    </body>

</html>