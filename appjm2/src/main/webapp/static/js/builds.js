$( "#form-search" ).on( "submit", function( event ) {
	var form = $( this ).serialize();
	$.post( "builds", form)
		.done( function (data) {
			$('#searched-builds').empty();
			$.each(data, function(i) {
				var build = getHTMLBuild(data[i]);
				$('#searched-builds').append(build);
			})
		});
	
	event.preventDefault();
});

function getHTMLBuild(build) {
	
	var div = document.createElement('div');
	var title = document.createElement('h3');
	var price = document.createElement('div');
	var comment = document.createElement('div');
	var img = document.createElement('img');
	var equipments = document.createElement('div');
	var features = document.createElement('div');
	var imgs = document.createElement('div');
	
	title.innerHTML = build.type + ' en ' + build.location + '<small>  ' + build.state + '</small>';
	price.innerHTML = build.price;
	comment.innerHTML = build.comment;
	comment.width = '400px';
	img.src = build.principalImagePath;
	img.width = '200px';
	img.style.float = 'right';
	
	var titleEquip = document.createElement('h5');
	titleEquip.innerHTML = 'Equipamientos';
	equipments.appendChild(titleEquip);
	var equips = build.equipments.split(',');
	equipments.style.float = 'left';
	equipments.style.paddingRight = '12px'
	for (var i in equips) {
		var equip = document.createElement('div');
		equip.innerHTML = equips[i];
		equipments.appendChild(equip);
	}
	
	var titleFeat = document.createElement('h5');
	titleFeat.innerHTML = 'Caracteristicas';
	features.appendChild(titleFeat);
	var feats = build.features.split(',');
	features.style.float = 'left';
	for (var i in feats) {
		var feat = document.createElement('div');
		feat.innerHTML = feats[i];
		features.appendChild(feat);
	}
	
	var images = build.imagesPath.split(',');
	for (var i in images) {
		var imgPath = document.createElement('img');
		imgPath.src = images[i];
		imgs.appendChild(imgPath);
	}
	
	div.appendChild(title);
	div.appendChild(price);
	div.appendChild(comment);
	price.appendChild(img);
	div.appendChild(equipments);
	div.appendChild(features);
	div.appendChild(imgs);
	
	div.padding = '200px';
	div.style.minHeight = '260px';
	
	return div;
}