package jm2.service.impl;

import java.util.List;

import jm2.dao.interfaces.BuildDao;
import jm2.model.BuildModel;
import jm2.model.SearchFormModel;
import jm2.service.interfaces.BuildService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BuildServiceImpl implements BuildService {

	@Autowired
	private BuildDao buildDao;
	
	@Override
	@Transactional
	public boolean saveBuild(BuildModel build) {
		return buildDao.saveBuild(build);
	}

	@Override
	@Transactional(readOnly = true)
	public List<BuildModel> findBuildsBySearchForm(SearchFormModel form) {
		return buildDao.findBuildsBySearchForm(form);
	}

}
