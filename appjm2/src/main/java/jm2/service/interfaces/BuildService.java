package jm2.service.interfaces;

import java.util.List;

import jm2.model.BuildModel;
import jm2.model.SearchFormModel;

public interface BuildService {

	boolean saveBuild(BuildModel build);
	
	List<BuildModel> findBuildsBySearchForm(SearchFormModel form);
	
}
