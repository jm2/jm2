package jm2.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ViewController {

	@RequestMapping(value = { "", "/*"})
	public String viewHandler() {
		return "index";
	}
	
	@RequestMapping(value = "/admin")
	public String adminHandler() {
		return "admin";
	}
	
	@RequestMapping(value = "/upload1")
	public String uploadHandler() {
		return "file-up";
	}
	
	@RequestMapping(value = "/image")
	public void imageHandler(
			HttpServletRequest request,
			@RequestParam("file") MultipartFile file) {
		
		String filePath = request.getServletContext().getRealPath("/");
		String destination = filePath + "/static/img/" + file.getOriginalFilename();
		
		try {
			File f = new File(destination);
			FileUtils.writeByteArrayToFile(f, file.getBytes());
			System.out.println("File:  " + f.toString() + " stored.");
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
