package jm2.controller;

import java.util.List;

import jm2.model.BuildModel;
import jm2.model.SearchFormModel;
import jm2.service.interfaces.BuildService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BuildController {
	
	@Autowired
	private BuildService buildService;
	
	@RequestMapping(value = "/builds", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<BuildModel> searchBuildsHandler(
			@ModelAttribute SearchFormModel form) {
		List<BuildModel> search = buildService.findBuildsBySearchForm(form);
		return search;
	}
	
}
