package jm2.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import jm2.model.BuildModel;
import jm2.service.interfaces.BuildService;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AdminController {

	@Autowired
	private BuildService buildService;
	
	@RequestMapping(value = "/upload")
	public String uploadHandler(
			@RequestParam("user") String user,
			@RequestParam("pass") String pass) {
		
		if(user.equals("alfonso") &&
			pass.equals("123456")) {
			return "upload";
		}
		
		return "admin";
	}
	
	private static int buildCount = 1;
	
	@RequestMapping(value = "/saveBuild")
	public String saveBuildHandler(
			HttpServletRequest request,
			@ModelAttribute BuildModel build,
			@RequestParam("file") MultipartFile file,
			@RequestParam("files") MultipartFile[] files) {
		
		build.setId(buildCount++);
		
		String filePath = request.getServletContext().getRealPath("/");
		String destination = filePath + "/static/img/" + file.getOriginalFilename();
		
		try {
			File f = new File(destination);
			FileUtils.writeByteArrayToFile(f, file.getBytes());
			System.out.println("File:  " + f.toString() + " stored.");
			build.setPrincipalImagePath("/app/static/img/" + file.getOriginalFilename());
			
			String imagesPath = "";
			for (MultipartFile fileImage : files) {
				destination = filePath + "/static/img/" + fileImage.getOriginalFilename();
				File fileImg = new File(destination);
				String filePathImg = "/app/static/img/" + fileImage.getOriginalFilename();
				FileUtils.writeByteArrayToFile(fileImg, fileImage.getBytes());
				imagesPath = imagesPath + filePathImg + ",";
			}
			build.setImagesPath(imagesPath);
			
			buildService.saveBuild(build);
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "upload";
	}
	
}
