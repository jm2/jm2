package jm2.model;

import java.io.Serializable;

public class SearchFormModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private String type;
	private String location;
	private String state;
	private String price;
	private String priceMin;
	private String priceMax;
	
	public SearchFormModel() { }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
		String priceMin = price.split("-")[0].trim();
		String priceMax = price.split("-")[1].trim();
		setPriceMin(priceMin);
		setPriceMax(priceMax);
	}

	public String getPriceMin() {
		if (priceMin == null || priceMin.equals("")) {
			setPriceMin("0");
		}
		return priceMin;
	}

	public void setPriceMin(String priceMin) {
		this.priceMin = priceMin;
	}

	public String getPriceMax() {
		if (priceMax == null || priceMax.equals("")) {
			setPriceMax("999999999999");
		}
		return priceMax;
	}

	public void setPriceMax(String priceMax) {
		this.priceMax = priceMax;
	}
	
}
