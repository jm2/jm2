package jm2.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "builds")
public class BuildModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/*
	 * Para recoger features y equipments bach de:
	 * 	Recoger build.
	 * 	Separar y insertar en tablas de feat. y equip.
	 * 	Comprobar si esta repetido, si lo esta cambiarle ese valory guardarlo.
	 */
	
	@Id
	private int id;
	private String price;
	private String comment;
	private Date lastUpdate;
	private String state;
	private String type;
	private String location;
	private String features;
	private String equipments;
	private String principalImagePath;
	private String imagesPath;
	
	public BuildModel() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getEquipments() {
		return equipments;
	}

	public void setEquipments(String equipments) {
		this.equipments = equipments;
	}

	public String getPrincipalImagePath() {
		return principalImagePath;
	}

	public void setPrincipalImagePath(String principalImagePath) {
		this.principalImagePath = principalImagePath;
	}

	public String getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}
	
}
