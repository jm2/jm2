package jm2.dao.impl;

import java.util.List;

import jm2.dao.GenericDao;
import jm2.dao.interfaces.BuildDao;
import jm2.model.BuildModel;
import jm2.model.SearchFormModel;

import org.springframework.stereotype.Repository;

@Repository
public class BuildDaoImpl extends GenericDao implements BuildDao {

	@Override
	public boolean saveBuild(BuildModel build) {
		boolean saved = false;
		
		try {
			getSession().save(build);
			saved = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return saved;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BuildModel> findBuildsBySearchForm(SearchFormModel form) {
		List<BuildModel> builds = null;
		String hql = "FROM BuildModel bm WHERE bm.type = :type AND bm.location = :location AND bm.state = :state AND bm.price BETWEEN :priceMin AND :priceMax";
		
		try {
			builds = getSession()
						.createQuery(hql)
							.setParameter("type", form.getType())
							.setParameter("location", form.getLocation())
							.setParameter("state", form.getState())
							.setParameter("priceMin", form.getPriceMin())
							.setParameter("priceMax", form.getPriceMax())
						.list();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return builds;
	}

}
