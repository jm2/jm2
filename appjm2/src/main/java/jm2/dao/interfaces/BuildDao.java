package jm2.dao.interfaces;

import java.util.List;

import jm2.model.BuildModel;
import jm2.model.SearchFormModel;

public interface BuildDao {

	boolean saveBuild(BuildModel build);
	
	List<BuildModel> findBuildsBySearchForm(SearchFormModel form);
	
}
